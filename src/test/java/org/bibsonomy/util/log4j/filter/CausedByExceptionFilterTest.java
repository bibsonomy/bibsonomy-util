package org.bibsonomy.util.log4j.filter;

import static org.junit.Assert.assertEquals;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.junit.Test;

/**
 * TODO: add documentation to this class
 *
 * @author sdo
 */
public class CausedByExceptionFilterTest {

	private static final String IAE = "java.lang.IllegalAccessError";
	private static final String RE = "java.lang.RuntimeException";
	private static final String ISE = "java.lang.IllegalStateException";

	/**
	 * Testing various cases of exceptions and their filtering
	 */
	@Test
	public void testDecide() {
		RuntimeException runArgStateAccEexception = new RuntimeException(new IllegalArgumentException(new IllegalStateException(new IllegalAccessError("Access was illegal"))));
		RuntimeException runArgAccException = new RuntimeException(new IllegalArgumentException(new IllegalAccessError("Access was even more illegal")));

		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter("SomeException", ISE, false));
		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter("SomeException", ISE, true));
		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter("SomeException", ISE, false, true));
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter("SomeException", ISE, false, true, true));
		
		
		CausedByExceptionFilter filter = buildFilter(RE, ISE, false);
		
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, filter);
		assertFilterStatus(Filter.Result.NEUTRAL, runArgAccException, filter);
		
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, ISE, false));
		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter(RE, ISE, true));
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, IAE, true));
	
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, IAE, false));

		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, IAE, false, false));
		
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, IAE, "Access was ill", false, false, false));
		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter(RE, IAE, "Access was kill", false, false, false));

		assertFilterStatus(Filter.Result.ACCEPT, runArgStateAccEexception, buildFilter(RE, IAE, "Access was ill", false, true, false));
		assertFilterStatus(Filter.Result.ACCEPT, runArgStateAccEexception, buildFilter(RE, IAE, "Access was ill", false, true, true));
		assertFilterStatus(Filter.Result.NEUTRAL, runArgStateAccEexception, buildFilter(RE, IAE, "Access was kill", false, true, false));
		assertFilterStatus(Filter.Result.DENY, runArgStateAccEexception, buildFilter(RE, IAE, "Access was kill", false, true, true));
	}
	
	private static CausedByExceptionFilter buildFilter(String ignoredException, String causeException, boolean rootCause) {
		return buildFilter(ignoredException, causeException, null, rootCause, false, false);
	}
	
	private static CausedByExceptionFilter buildFilter(String ignoredException, String causeException, boolean rootCause, boolean invert) {
		return buildFilter(ignoredException, causeException, null, rootCause, invert, false);
	}

	private static CausedByExceptionFilter buildFilter(String ignoredException, String causeException, boolean rootCause, boolean invert, boolean last) {
		return buildFilter(ignoredException, causeException, null, rootCause, invert, last);
	}

	private static CausedByExceptionFilter buildFilter(String ignoredException, String causeException, String causeExceptionMessageBegin, boolean rootCause, boolean invert, boolean last) {
		return new CausedByExceptionFilter(ignoredException, causeException, causeExceptionMessageBegin, rootCause, invert, last);
	}
	
	
	private static void assertFilterStatus(Filter.Result expected, Throwable throwable, Filter filter) {
		final Log4jLogEvent.Builder builder = new Log4jLogEvent.Builder();
		builder.setThrown(throwable);
		builder.setLevel(Level.ERROR);
		assertEquals(expected, filter.filter(builder.build()));
	}
}
