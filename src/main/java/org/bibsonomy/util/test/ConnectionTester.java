package org.bibsonomy.util.test;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * tests an url connection
 * @author dzo
 */
public class ConnectionTester {
	
	/**
	 * @param args 
	 * 			[0] the url to test 
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		final String url = args[0];
		final URLConnection connection = new URL(url).openConnection();
		if (args.length > 1) {
			connection.setRequestProperty("Authorization", "Basic " + args[1]);
		}
		connection.setRequestProperty("Accept-Charset", "UTF-8");
		final InputStream response = connection.getInputStream();
		response.close();
	}
}
