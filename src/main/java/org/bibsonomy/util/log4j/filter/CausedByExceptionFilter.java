package org.bibsonomy.util.log4j.filter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.message.Message;

/**
 * Ignore an exception of a given class only when a given other exception is causing it.
 * @author sdo
 */
@Plugin(name = "CausedByExceptionFilter", category = "Core", elementType = "filter")
public class CausedByExceptionFilter extends AbstractFilter {
	/** A maximum depth for the iteration through the chain of causes. */
	private static final int MAX_DEPTH = 50;

	private final String ignoredException;

	private final String causeException;

	private final String causeExceptionMessageBegin;

	private final boolean rootCause;

	private final boolean invert;

	private final boolean last;

	public CausedByExceptionFilter(String ignoredException, String causeException, String causeExceptionMessageBegin, boolean rootCause, boolean invert, boolean last) {
		this.ignoredException = ignoredException;
		this.causeException = causeException;
		this.causeExceptionMessageBegin = causeExceptionMessageBegin;
		this.rootCause = rootCause;
		this.invert = invert;
		this.last = last;
	}

	@Override
	public Result filter(LogEvent event) {
		return this.filterByCausedBy(event.getThrown());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Object msg, Throwable t) {
		return this.filterByCausedBy(t);
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Message msg, Throwable t) {
		return this.filterByCausedBy(t);
	}

	private Result filterByCausedBy(Throwable throwable) {
		if (throwable != null) {
			if (this.ignoredException != null && this.ignoredException.equals(throwable.getClass().getName())) {
				/*
				 * go through the causing exceptions chain one by one and check if the causing Exception is of the type causeException
				 * if rootCause is true, we are only interested in the root causing exception
				 */
				if (causeException != null) {
					int depth = 0;
					while (throwable != null && depth < MAX_DEPTH) {
						depth++;
						final Throwable cause = throwable.getCause();
						if ((!rootCause || cause == null) && causeException.equals(throwable.getClass().getName())) {
							if (causeExceptionMessageBegin == null || causeExceptionMessageBegin.isEmpty()) {
								return getFinalDecision(Result.DENY);
							}
							if (throwable.getMessage().startsWith(causeExceptionMessageBegin)) {
								return getFinalDecision(Result.DENY);
							}
						}
						if (cause == throwable) {
							break;
						}
						throwable = cause;
					}
				}
			}
		}
		return this.getFinalDecision(Result.NEUTRAL);
	}

	/**
	 * When we do not invert we just return the decision.
	 * In the inverted case
	 * * when the filter would DENY (pattern fits) we ACCEPT it and thus it will be logged.
	 * * when the filter is NEUTRAL (pattern does not fit) we DENY it if this is the last filter, otherwise we are NEUTRAL and thus leave the decision for the next filter
	 * @param filterResult
	 * @return
	 */
	private Result getFinalDecision(Result filterResult) {
		if (!this.invert) {
			return filterResult;
		}
		if (Result.DENY.equals(filterResult)) {
			return Result.ACCEPT;
		}
		// filter == NEUTRAL
		return this.last ? Result.DENY : Result.NEUTRAL;
	}

	@PluginFactory
	public static CausedByExceptionFilter createFilter(@PluginAttribute("ignoredException") String ignoredException, @PluginAttribute("causeException") String causeException, @PluginAttribute("causeExceptionMessageBegin") String causeExceptionMessageBegin, @PluginAttribute("rootCause") boolean rootCause, @PluginAttribute("invert") boolean invert, @PluginAttribute("last") boolean last) {
		return new CausedByExceptionFilter(ignoredException, causeException, causeExceptionMessageBegin, rootCause,  invert, last);
	}

}
