package org.bibsonomy.util.log4j.filter;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.message.Message;

/**
 * 
 * @author dzo
 */
@Plugin(name = "ExceptionFilter", category = "Core", elementType = "filter", printObject = true)
public final class ExceptionFilter extends AbstractFilter {
	
	@Plugin(name = "IgnoredException", category = "Core")
	public static final class IgnoredExceptionWrapper {

		private final String name;

		public IgnoredExceptionWrapper(String name) {
			this.name = name;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		@PluginFactory
		public static IgnoredExceptionWrapper createIgnoredException(@PluginAttribute("name") final String name) {
			return new IgnoredExceptionWrapper(name);
		}
	}

	private final Set<String> ignoredExceptions;

	public ExceptionFilter(Set<String> ignoredExceptions) {
		this.ignoredExceptions = ignoredExceptions;
	}

	@Override
	public Result filter(final LogEvent event) {
		final Throwable thrown = event.getThrown();
		return this.filterException(thrown);
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Object msg, Throwable t) {
		return this.filterException(t);
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Message msg, Throwable t) {
		return this.filterException(t);
	}

	private Result filterException(Throwable thrown) {
		if (thrown != null) {
			final Class<? extends Throwable> thrownClass = thrown.getClass();

			if (this.ignoredExceptions.contains(thrownClass.getName())) {
				return Result.DENY;
			}
		}
		return Result.NEUTRAL;
	}

	@Override
	public String toString() {
		return this.ignoredExceptions.toString();
	}

	@PluginFactory
	public static ExceptionFilter createFilter(@PluginElement("IgnoredException") IgnoredExceptionWrapper[] ignoredExceptions) {
		final Stream<String> stream = Arrays.stream(ignoredExceptions).map(IgnoredExceptionWrapper::getName);
		return new ExceptionFilter(stream.collect(Collectors.toSet()));
	}
}
